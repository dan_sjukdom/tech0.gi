FROM node:14.15.3-alpine3.12

ENV PORT 3000
EXPOSE 3000

WORKDIR /app

COPY package.json .
COPY yarn.lock .

RUN yarn install

COPY . .

CMD ["yarn", "start"]