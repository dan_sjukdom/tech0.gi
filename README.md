# Tech 0

This backend server is writen using express.js and it's compiled using babel to use the ES6 new syntax. It's as easy as:

* Defining a .babelrc file with:
```js
{
    "presets": [
        "@babel/preset-env"
    ]
}
```

* Compile the code before running it, defined in package.json:
```json
  "scripts": {
    "start": "nodemon --exec babel-node src/index.js"
  }
```

## Deployment

I did the deployment using Heroku, a PaaS service where you can deploy apps in a non painful way and it offers a free plan
with some limitations but really useful for test purposes:

https://tech0.herokuapp.com/


A user that already exists and can be used to authenticate and make requests to the server is the following:
```shell
$ curl -X POST -d "email=tylerdurden@gmail.com&password=tylerdurden" https://tech0.herokuapp.com/api/user/signin/
$ {"jwt":"eyJhbGciOiJSUzI1NiIsImtpZCI6ImUwOGI0NzM0YjYxNmE0MWFhZmE5MmNlZTVjYzg3Yjc2MmRmNjRmYTIiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vdGVjaDAtMjBiYzgiLCJhdWQiOiJ0ZWNoMC0yMGJjOCIsImF1dGhfdGltZSI6MTYxMDEyMDYyNywidXNlcl9pZCI6IkNYdFF6QVc5b1JnVmluUVJ1V1F0OGhVSTlVSzIiLCJzdWIiOiJDWHRRekFXOW9SZ1ZpblFSdVdRdDhoVUk5VUsyIiwiaWF0IjoxNjEwMTIwNjI3LCJleHAiOjE2MTAxMjQyMjcsImVtYWlsIjoidHlsZXJkdXJkZW5AZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7ImVtYWlsIjpbInR5bGVyZHVyZGVuQGdtYWlsLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.hHaDUES77gM3TLmD3VLOp75KT9J_scINAMiSBNFBmAeYhHp6nZh3mEKBshM0zHg8PTO4kizEoHny5mykGd0G9NKqAS_1WYl-xT7qVJj2uZz-Tebr1eqzj0ckRBRJ_rmtPZ5_A71S45VUHYIEeBC-o_lYdZvKH0-jxdiU4S-dZTxpqTe49XF6nU3sTRBsFuMOSi3leuvd4yiVfG4Ns1YAINCZxuG53ylZYfMzJA0WFV-RL3GdKtwqQdzbdzdiQEQDmHx1NBR0Rb4_5cLfmV-p76YoI7RyUuFaJE4ahAenYGypoD9RXLnFDi6r4wz7_0t_zZUOHkQcZqRqzaYiAlBc9w"}
```

Then the token is used in the authorization header:
```shell
$ curl -H "Authorization: Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6ImUwOGI0NzM0YjYxNmE0MWFhZmE5MmNlZTVjYzg3Yjc2MmRmNjRmYTIiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vdGVjaDAtMjBiYzgiLCJhdWQiOiJ0ZWNoMC0yMGJjOCIsImF1dGhfdGltZSI6MTYxMDEyNzczMSwidXNlcl9pZCI6IkNYdFF6QVc5b1JnVmluUVJ1V1F0OGhVSTlVSzIiLCJzdWIiOiJDWHRRekFXOW9SZ1ZpblFSdVdRdDhoVUk5VUsyIiwiaWF0IjoxNjEwMTI3NzMxLCJleHAiOjE2MTAxMzEzMzEsImVtYWlsIjoidHlsZXJkdXJkZW5AZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7ImVtYWlsIjpbInR5bGVyZHVyZGVuQGdtYWlsLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.EnXzm_G4raPkP_tG9dQL0XzXH1hEa_MfnlN2udQZnbQgrU6ALQ4yjQerjUpU8hJStVgjyL3TCnDgI9l7Cw0fzD80-CDSYf8hwqsjsqYCP7CiDadLIoCKkelYC_KGtoKSSiEo5Fg7U76PkMukgGxVbaxGIPYupMYzmSLHbEWjqOByMDU8mNu_WrsgU_j6zGHihldDcYZhwLZJEglu7SFcnyGtpYeynTlurSjx7nm_G4mEAoqN-biei5lWSLuREi9tTWEOynyjve2ajuI9n07ak5qxTb4I95pI6ltAzw1BlTFUG7sngdCMxZwtJs4M4xRWcfrcoefcmNXTO9dE0DvdsA" https://tech0.herokuapp.com/api/user/inventory/detail/
```

Also, a card can be created, make sure the image it's a path to an existing file (also make sure to use this request to post a card locally):
```shell
$ How 3curl -X POST -H "Authorization: Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6ImUwOGI0NzM0YjYxNmE0MWFhZmE5MmNlZTVjYzg3Yjc2MmRmNjRmYTIiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vdGVjaDAtMjBiYzgiLCJhdWQiOiJ0ZWNoMC0yMGJjOCIsImF1dGhfdGltZSI6MTYxMDEyNzczMSwidXNlcl9pZCI6IkNYdFF6QVc5b1JnVmluUVJ1V1F0OGhVSTlVSzIiLCJzdWIiOiJDWHRRekFXOW9SZ1ZpblFSdVdRdDhoVUk5VUsyIiwiaWF0IjoxNjEwMTI3NzMxLCJleHAiOjE2MTAxMzEzMzEsImVtYWlsIjoidHlsZXJkdXJkZW5AZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7ImVtYWlsIjpbInR5bGVyZHVyZGVuQGdtYWlsLmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.EnXzm_G4raPkP_tG9dQL0XzXH1hEa_MfnlN2udQZnbQgrU6ALQ4yjQerjUpU8hJStVgjyL3TCnDgI9l7Cw0fzD80-CDSYf8hwqsjsqYCP7CiDadLIoCKkelYC_KGtoKSSiEo5Fg7U76PkMukgGxVbaxGIPYupMYzmSLHbEWjqOByMDU8mNu_WrsgU_j6zGHihldDcYZhwLZJEglu7SFcnyGtpYeynTlurSjx7nm_G4mEAoqN-biei5lWSLuREi9tTWEOynyjve2ajuI9n07ak5qxTb4I95pI6ltAzw1BlTFUG7sngdCMxZwtJs4M4xRWcfrcoefcmNXTO9dE0DvdsA" -F "image=@/home/sjukdom/Pictures/Narasimhadev/Wallpaper/Meditalien.jpeg" -F "title=How 2 survive v2" -F "price=144.44" -F "tags=alien" https://tech0.herokuapp.com/api/user/inventory/add/
```

## Install and run locally

CLlone the repository with your favorite method, then whether or not the server is executed with docker it runs in the socket 0.0.0.0:3000. 

* Without docker:
```shell
$ yarn install
$ yarn start
```

* With docker:

Make sure you have docker and docker-compose installed, then the container can run:

* The easy way
```shell
$ sudo docker-compose up -d
```

* The "hard" way:
```shell
$ sudo docker build -t tech0 .
$ sudo docker run -it -d --rm -p 3000:3000 --name tech0 tech0
```


## Design 

The first part after coding is the design of the solution:
* Database schema
* REST API architecture

### Database

The database I used is ___firestore__. I started designing the database schema using an entity relationship diagram and then 
I turned it into a no-sql approach, where:

* Table is a collection
* Row is a document
* Column is a field


I did the ERD using an online free tool available at [dbdiagram](https://dbdiagram.io).

![Database Schema](media/dbshema.png)

The diagram is defined with the following code:

```
Table user {
  id string [pk]
  username varchar
  email email
  password varchar
}


Table card {
  id string [pk]
  title varchar
  price float
  image varchar
  inventory string
}


Table inventory {
  id string
  user string
  Indexes {
    (id, user) [pk]
  }
}


Table tag {
  id string [pk]
  name varchar
}


Table cardHasTag {
  id string
  tag string
  card string
  Indexes {
    (id, tag, card) [pk]
  }
}



Ref: user.id - inventory.user
Ref: inventory.id > card.inventory
Ref: card.id < cardHasTag.card
Ref: tag.id > cardHasTag.tag
```

The nosql version is quite different, the main difference is that the inventory is a collection where each document has a list of references to the cards that has it.

```
User = [
  ...,
  {
    id: <id>,
    username: <username>,
    email: <email>
  },
  ...
]

Tag = [
  ...,
  {
    id: <id>,
    name: <name>
  },
  ...
]

Card = [
  ...,
  {
    id: <id>
    title: <title>,
    price: <price>,
    tags: [
      <tag_reference_1>,
      ...,
      <tag_reference_n>
    ]
  }
  ...
]

Inventory = [
  ...,
  {
    id: <id>,
    user: <user_reference>,
    cards: [
      <card_reference_1>,
      ...,
      <card_reference_n>
    ]
  },
  ...
]
```


### Rest API

Express.js as the backend framework to handle the HTTP requests.

To documment it I used __Postman__: https://documenter.getpostman.com/view/12667304/TVzPmddZ.

## Resources

To create this app I used this resources and in general all the google's firebase docs which where really useful.

[Admin SDK]()

[Client SDK](https://firebase.google.com/docs/web/setup#node.js-apps)

[Verify token](https://firebase.google.com/docs/auth/admin/verify-id-tokens#node.js)

[Add a document (custom id)](https://firebase.google.com/docs/firestore/manage-data/add-data)

[Admin storage bucket](https://firebase.google.com/docs/storage/admin/start)

[File write stream](https://googleapis.dev/nodejs/storage/latest/global.html#CreateWriteStreamOptions)

[Upload a file using buckets](https://googleapis.dev/nodejs/storage/latest/Bucket.html#file)