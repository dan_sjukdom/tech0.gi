import {
    admin
} from "../firebase/admin";


const isAuthenticated = async (req, res, next) => {
    /*
        Verify if the request has the authorization header and whether or not
        the bearer token is valid and isn't expired.
    */
    if ("authorization" in req.headers) {
        const token = req.headers.authorization.split(" ")[1];
        admin.auth().verifyIdToken(token)
        .then( decodedToken => {
            console.log(`Authenticated user: ${decodedToken.email}`);
            next();
        })
        .catch(err => {
            res.status(401).json({
                code: err.code,
                message: err.message
            });
        });
    }
    else {
        res.status(401).json({
            message: "The request is not authorized"
        })
    }
};

export {
    isAuthenticated
};