import {
    admin,
    db,
    firebase
 } from "../firebase/admin";
import path from "path";
import stream from "stream";


export const UserAPI = async (req, res) => {
    /*
        Fecth all the existing users in firebase -> firestore
    */
    try {
        const query = await db.collection("/users").get();
        res.json(query.docs.map(doc => doc.data()));
    }
    catch (err) {
        res.status(400).json({
            message: "There was an error making the query"
        });
    }
};


export const UserDetailAPI = async (req, res) => {
    /*
        Retrieve an user from firebase (if it exists)
        querying by the email address.
    */

    try {
        const email = req.params.email;
        const query = await db.collection("/users").where("email", "==", email);
        query.get()
        .then(doc => {
            if(doc.empty) {
                res.status(400).json({
                    message: "There user doesn't exists"
                });
            } 
            else {
                const data = doc.docs[0].data();
                res.json(data);
            }
        })
        .catch(err => {
            res.status(400).json({
                message: err.message
            });
        });
    }
    catch (err) {
       res.status(400).json({
        message: err.message
       });
    }
};


export const UserCreateAPI = async (req, res) => {
    /*
        An user is created via firebase if and only if:
            * The email is not repeated
            * The username is not repeated
        and then a document in the user collection 
        (at firestore) is created with the same id.

        An inventory document is created for the user.
    */

    try {

        const email = req.body.email.trim();
        const password = req.body.password;
        const username = req.body.username;

        const query = await db.collection("/users").select("username").where(`username`, "==", `${username}`);

        query.get()
        .then(doc => {
            if (doc.empty) {
                firebase.auth().createUserWithEmailAndPassword(email, password)
                .then(firebase_user => {
                    const uid = firebase_user.user.uid;
                    db.collection("/users").doc(`${uid}`).set({
                        email: email,
                        username: username, 
                    })
                    .then(inserted_user => {
                        db.collection("/inventory").doc(`${uid}_inventory`).set({
                            user: db.doc(`/users/${uid}`),
                            cards: []
                        })
                        .then(() => {
                            res.status(201).json({
                                message: `The user was created successfully with the id ${uid}`
                            })
                        })
                        .catch(err => {
                            res.status(400).json({
                                code: err.code,
                                message: err.message
                            })
                        });
                    })
                    .catch(err => {
                        res.status(400).json({
                            message: "There was an error creating the document"
                        });
                    });
                })
                .catch(err => {
                    res.status(400).json({
                        code: err.code,
                        message: err.message
                    })
                })
            }
            else {
                res.status(400).json({
                    message: `A user with the username ${username} already exists`
                });
            }})
        .catch(err => {
            res.json("There was an error using the query")
        });
    }
    catch (err) {
        res.status(400).json({
            message: err.code
        })
    }
}


export const UserLoginAPI = async (req, res) => {
    /*
        Sign the user in using the email and password
        as credentials.
    */
    try {
        const email = req.body.email.trim();
        const password = req.body.password;
        const user_credential = await firebase.auth().signInWithEmailAndPassword(email, password);
        const token = await user_credential.user.getIdToken(true);
        res.json({
            "jwt": token.toString()
        });
    }
    catch (err) {
        res.status(400).json({
            code: err.code,
            message: err.message
        });
    }
};


export const UserDeleteAPI = (req, res) => {
    /*
        The current authenticated user deletes the account 
        and also their inventory and cards are deleted.
    */
    try {
        const jwt = req.headers.authorization.split(" ")[1];
        admin.auth().verifyIdToken(jwt).then(user => {
            const uid = user.uid;
            admin.auth().deleteUser(uid)
            .then(() => {
                db.collection("/users").doc(`${uid}`).delete()
                .then(() => {
                    db.collection("/inventory").doc(`${uid}_inventory`).delete()
                    .then(() => {
                        res.json({
                            message: "The user, its inventory and cards were deleted succesfully"
                        });
                    })
                    .catch(err => {
                        res.status(400).json({
                            code: err.code,
                            message: err.message
                        });
                    })
                })
                .catch(err => {
                    res.status(400).json({
                        code: err.code,
                        message: err.message
                    });
                })
            })
            .catch(err => {
                res.json({
                    code: err.code,
                    message: err.message
                });
            })
        })
    }
    catch (err) {
        res.status(400).json({
            message: err.message
        });
    }
};


export const UserDeleteCard = (req, res) => {
    /*
        Delete the card with with the given id if and only if
        the user authenticated is the owner. Removes first the 
        reference from the inventory and then the card document.
    */
    try {
        const card_id = req.params.id;
        const jwt = req.headers.authorization.split(" ")[1];
        admin.auth().verifyIdToken(jwt).then(user => {
            db.collection("/inventory").doc(`${user.uid}_inventory`).get()
            .then(snapshot => {
                snapshot.data().cards.forEach(card_ref => {
                    if(card_ref.id === card_id) {
                        card_ref.get()
                        .then(snap => {
                            const image_name = path.basename(snap.data().image);
                            admin.storage().bucket().file(image_name).delete();
                            card_ref.delete()
                            .then(() => {
                                res.json({
                                    message: "The card was deleted successfully"
                                })
                            })
                            .catch(err => res.status(400).json({message: err.message}))
                        })
                        .catch(err => res.status(400).json({message: err.message}))
                    }
                })
            })
            .catch(err => {
                res.status(400).json({
                    message: err.message
                });
            })
        });
    }
    catch (err) {
        res.status(400).json({
            message: err.message
        });
    }
};


export const UserUpdateCard = (req, res) => {
    /*
        Since this is a PUT method, the function expects to update 
        all the field in the card document. Only the owner of the card
        is allow to perform this action.
        Update field expected:
        * title
        * price
    */
    try {
        const title = req.body.title;
        const price = req.body.price;
        const card_id = req.params.id;

        const jwt = req.headers.authorization.split(" ")[1];
        admin.auth().verifyIdToken(jwt).then(user => {
            db.collection("/inventory").doc(`${user.uid}_inventory`).get()
            .then(snapshot => {
                snapshot.data().cards.forEach(card_ref => {
                    if (card_ref.id === card_id) {
                        console.log("User is allowed to update the card's content");
                        card_ref.update({
                            title: title,
                            price: price
                        })
                        .then(() => {
                            card_ref.get().then(snap => {
                                res.json({ 
                                    message: "Card was updated successfully",
                                    card: snap.data()
                                });
                            });
                        })
                        .catch(err => res.status(400).json({message: err.message}))
                    }
                })
            })
            .catch(err => res.status(400).json({message: err.message}))
        });
    }
    catch (err) {
        res.status(400).json({
            message: err.message
        });
    }
};


export const UserInventoryAPI = (req, res) => {
    /*
        Get all the cards in the authenticated user inventory.
        A optional filter can be added as a query parameter:
        * tag
    */
    
    try {

        const jwt = req.headers.authorization.split(" ")[1];
        
        admin.auth().verifyIdToken(jwt)
        .then(user => {
            // Access the user inventory and then resolve the document snapshot by
            // getting the card's field and for each reference get the data.
            const userRef = db.collection("/users").doc(user.uid);
            let query = db.collection("/inventory").where("user", "==", userRef);

            query.get()
            .then(snapshot => {
                const promises = snapshot.docs.map(doc => doc.data().cards.map(cardRef => {
                    return cardRef.get()
                    .then(cardDoc => new Promise((resolve, reject) => resolve(cardDoc.data())))
                }));
                Promise.all(promises[0])
                .then(result => res.json(result))
                .catch(err => res.status(400).json({ message: err.message }))
            })
            .catch(err => console.log(err))
        })
        .catch(err => {
            res.status(400).json({
                code: err.code,
                message: err.message
            })
        })
    }
    catch (err) {
        res.status(400).json({
            message: err.message
        })
    }
};


export const UserInventoryAddAPI = async (req, res) => {
    /*
        Add an item (card) to the user inventory.
        The image is read and encoded as a base64 string using a 
        filesystem read stream in which the content is uplodaded to
        firebase store and the image uri is stored inside the 
        user document.
    */

    try {

        const title = req.body.title;
        const price = parseFloat(req.body.price);
        // const image_path = req.body.image;

        // console.log(req)
        const image = req.file;
        console.log(image.originalname)
        console.log(req.body)

        let tags;
        if ("tags" in req.body) {
            tags = req.body.tags.split(",");
        };

        const image_name = image.originalname;

        const bucket = admin.storage().bucket();
        const image_file = bucket.file(image_name);

        const jwt = req.headers.authorization.split(" ")[1];
        const user = await admin.auth().verifyIdToken(jwt);

        const buffer = new stream.PassThrough();
        buffer.end(Buffer.from(image.buffer))
        .pipe(
            image_file.createWriteStream({
                public: true,
                metadata: {
                    contentEncoding: "base64"
                }
            })
            .on("error", (err) => {
                res.status(400).json({
                    message: err.message
                });
            })
            .on("finish", () => { 
                const image_uri = `gs://${bucket.name}/${image_name}`;
                // Create a document for each tag
                // if it exists already, skip it.
                if (tags) {
                    tags.forEach(tag => {
                        db.collection("/tag").where("name", "==", tag.trim()).get()
                        .then(doc => {
                            if (doc.empty) {
                                db.collection("/tag").doc(`tag_${tag.trim()}`).set({
                                    name: tag.trim()
                                });
                            }
                        })
                        .catch(err => {
                            res.status(400).json({
                                code: err.code,
                                message: err.message
                            });
                        });
                    });
                }
                // Create the card document and then add its 
                // reference to the user inventory
                db.collection("/card").add({
                    title: title,
                    price: price,
                    image: image_uri,
                    tags: tags.map(tag => `tag_${tag.trim()}`)
                })
                .then(doc => {
                    // Update the user inventory document by inserting the
                    // new card in the list of references
                    const card_id = doc.id;
                    db.collection("/inventory").doc(`${user.uid}_inventory`).update({
                        cards: admin.firestore.FieldValue.arrayUnion(db.doc(`/card/${card_id}`))
                    })
                    .then(() => {
                        res.status(201).json({
                            message: `A new card was created with id ${card_id}`
                        })
                    })
                    .catch(err => {
                        res.status(400).json({
                            code: err.code,
                            message: err.message
                        });
                    });
                })
                .catch(err => {
                    res.status(400).json({
                        message: err.message
                    });
                })
            })
        );
    }
    catch (err) {
        res.status(400).json({
            message: err.message
        });
    }
};