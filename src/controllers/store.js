import e from "express";
import {
    admin,
    db,
    firebase
} from "../firebase/admin";


export const ItemsAPI = async (req, res) => {

    /*
        Fetch all items if no query parameters are specified.
        The allowed params are:
        * tag (single, multiple)
        * price (==, <, <=, >=, >)
    */

    let tags;
    let price;
    let price_min;
    let price_max;

    if ("tag" in req.query) {
        tags = req.query.tag;
        if (typeof(tags) == "string") {
            tags = `tag_${tags}`;
        }
        else {
            tags = tags.map(tag => `tag_${tag}`);
        }
    }
    if ("price" in req.query) {
        price = parseFloat(req.query.price);
    }
    else {
        if ("price_min" in req.query) {
            price_min = parseFloat(req.query.price_min);
        }
        if ("price_max" in req.query) {
            price_max = parseFloat(req.query.price_max);
        }
    };

    try {
        let query = db.collection("/card");

        if(tags) {
            query = query.where("tags", "array-contains-any", tags);
        }

        if (price) {
            query = query.where("price", "==", price);
        }

        else {
            if (price_max) {
                query = query.where("price", "<", price_max);
            }
            if (price_min) {
                query = query.where("price", ">", price_min);
            }
        }

        query.get()
        .then(snapshot => {
            const documents = snapshot.docs.map(doc => doc.data());
            res.json(documents);
        })
        .catch(err => {
            res.status(400).json({
                message: err.message
            });
        });
    }
    catch (err) {
        res.status(400).json({
            code: err.code,
            message: err.message
        });
    }
};


export const ItemsDetailAPI = async (req, res) => {

    /*
        Fetch the card with the specified id (if it exists)
    */

    const card_id = req.params.id;
    
    try {
        const query = await db.collection("/card").doc(card_id).get();
        res.json(query.data());
    }
    catch (err) {
        res.status(400).json({
            message: err.message
        })
    }
};