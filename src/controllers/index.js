import path from "path";


export const index = (req, res, next) => {
    res.sendFile(path.join(__dirname, "./../views/index.html"));
};