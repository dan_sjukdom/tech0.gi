import express from "express";
import bodyParser from "body-parser";
import morgan from "morgan";
import IndexRouter from "./routes/index";
import StoreRouter from "./routes/store";
import UserRouter from "./routes/users";

const app = express();


app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(morgan("dev"));

app.get("/", IndexRouter);
app.use("/api/store", StoreRouter);
app.use("/api/user", UserRouter);


const PORT = process.env.PORT || 3000;


app.listen(PORT, () => {
    console.log(`Listening on port ${PORT}`);
});