import express from "express";
import {
    ItemsAPI,
    ItemsDetailAPI
} from "../controllers/store";
import { isAuthenticated } from "../middleware/auth";


const router = express.Router();


router.get("/items/", isAuthenticated, ItemsAPI);
router.get("/items/:id/", isAuthenticated, ItemsDetailAPI);



export default router;