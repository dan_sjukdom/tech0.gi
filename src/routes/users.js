import express from "express";
import {
    UserAPI,
    UserDetailAPI,
    UserCreateAPI,
    UserLoginAPI,
    UserDeleteAPI,
    UserInventoryAPI,
    UserInventoryAddAPI,
    UserDeleteCard,
    UserUpdateCard
} from "../controllers/users";
import {
    isAuthenticated
} from "../middleware/auth";
import multer from "multer";


const router = express.Router();
const upload = multer({});

// User actions
router.get("/", isAuthenticated, UserAPI);
router.post("/create/", UserCreateAPI);
router.post("/signin/", UserLoginAPI);
router.delete("/delete/", isAuthenticated, UserDeleteAPI);

// User detail
router.get("/:email/", isAuthenticated, UserDetailAPI);

// Inventory actions
router.post("/inventory/add/", isAuthenticated, upload.single("image"), UserInventoryAddAPI);
router.get("/inventory/detail/", isAuthenticated, UserInventoryAPI);
router.delete("/inventory/card/:id/delete/", isAuthenticated, UserDeleteCard);
router.put("/inventory/card/:id/update/", isAuthenticated, UserUpdateCard);

export default router;