import admin from "firebase-admin";
import {
    serviceAccount,
    firebaseApp
} from "./config";
import firebase from "firebase";


admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    storageBucket: "tech0-20bc8.appspot.com"
});


firebase.initializeApp(firebaseApp);

const db = admin.firestore();

export {
    admin,
    firebase,
    db
};